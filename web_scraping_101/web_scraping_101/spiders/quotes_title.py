import scrapy


# This spider scrapes just the title of the web page.
class QuoteSpider(scrapy.Spider):
    name = 'quotes_title'
    start_urls = ['http://quotes.toscrape.com', ]

    def parse(self, response):
        title = response.css('title::text').extract()
        yield {'title_text': title}
