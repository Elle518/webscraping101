import scrapy
from ..items import WebScraping101Item


# This spider scrapes all the pages of the web
# using pagination to obtain the next page.
class QuotesSpider(scrapy.Spider):
    name = 'quotes_pagination'
    page_number = 2
    start_urls = ['http://quotes.toscrape.com/page/1/']

    def parse(self, response):
        items = WebScraping101Item()
        all_quotes = response.css('div.quote')

        for quote in all_quotes:
            quote_sentence = quote.css('span.text::text').extract()
            author = quote.css('.author::text').extract()
            tags = quote.css('.tag::text').extract()

            items['quote_sentence'] = quote_sentence
            items['author'] = author
            items['tags'] = tags

            yield items

        next_page = 'http://quotes.toscrape.com/page/' + str(QuotesSpider.page_number) + '/'

        if QuotesSpider.page_number < 11:
            QuotesSpider.page_number += 1
            yield response.follow(next_page, callback=self.parse)
