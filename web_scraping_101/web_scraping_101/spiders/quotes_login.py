import scrapy
from scrapy.http import FormRequest
from ..items import WebScraping101Item
from scrapy.utils.response import   open_in_browser


class QuotesSpider(scrapy.Spider):
    name = 'quotes_login'
    page_number = 2
    start_urls = ['http://quotes.toscrape.com/login']

    def parse(self, response):
        token = response.css('form input::attr(value)').extract_first()
        return FormRequest.from_response(response, formdata={
            'csrf_token': token,
            'username': 'teregc@hotmail.es',
            'password': '12345'
        }, callback=self.start_scraping)

    def start_scraping(self, response):
        open_in_browser(response)
        items = WebScraping101Item()
        all_quotes = response.css('div.quote')

        for quote in all_quotes:
            quote_sentence = quote.css('span.text::text').extract()
            author = quote.css('.author::text').extract()
            tags = quote.css('.tag::text').extract()

            items['quote_sentence'] = quote_sentence
            items['author'] = author
            items['tags'] = tags

            yield items
