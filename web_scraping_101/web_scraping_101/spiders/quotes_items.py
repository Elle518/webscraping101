import scrapy
from ..items import WebScraping101Item


# This spider scrapes all the quotes on the first page of
# the web and uses Item class to temporally store the content.
class QuotesSpider(scrapy.Spider):
    name = 'quotes_items'
    start_urls = ['http://quotes.toscrape.com', ]

    def parse(self, response):
        items = WebScraping101Item()
        all_quotes = response.css('div.quote')

        for quote in all_quotes:
            quote_sentence = quote.css('span.text::text').extract()
            author = quote.css('.author::text').extract()
            tags = quote.css('.tag::text').extract()

            items['quote_sentence'] = quote_sentence
            items['author'] = author
            items['tags'] = tags

            yield items
