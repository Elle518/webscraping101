import scrapy


# This spider scrapes first all the quotes of the first page
# in one list, then the all the authors and finally the tags.
class QuoteSpider(scrapy.Spider):
    name = 'quotes_mixed'
    start_urls = ['http://quotes.toscrape.com', ]

    def parse(self, response):
        all_quotes = response.css('div.quote')
        quote_sentence = all_quotes.css('span.text::text').extract()
        author = all_quotes.css('.author::text').extract()
        tag = all_quotes.css('.tag::text').extract()
        yield {'quote': quote_sentence,
               'author': author,
               'tag': tag
               }
