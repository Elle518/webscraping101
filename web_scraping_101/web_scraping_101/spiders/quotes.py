import scrapy


# This spider scrapes one by one all the quotes on the
# first page of the web with their author and tags.
class QuoteSpider(scrapy.Spider):
    name = 'quotes'
    start_urls = ['http://quotes.toscrape.com', ]

    def parse(self, response):
        all_quotes = response.css('div.quote')

        for quote in all_quotes:
            quote_sentence = quote.css('span.text::text').extract()
            author = quote.css('.author::text').extract()
            tag = quote.css('.tag::text').extract()
            yield {'quote': quote_sentence,
                   'author': author,
                   'tag': tag}
