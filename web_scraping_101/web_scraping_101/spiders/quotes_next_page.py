import scrapy
from ..items import WebScraping101Item


# This spider scrapes all the pages of the web
# using selectors to obtain the next page.
class QuotesSpider(scrapy.Spider):
    name = 'quotes_next_page'
    start_urls = ['http://quotes.toscrape.com', ]

    def parse(self, response):
        items = WebScraping101Item()
        all_quotes = response.css('div.quote')

        for quote in all_quotes:
            quote_sentence = quote.css('span.text::text').extract()
            author = quote.css('.author::text').extract()
            tags = quote.css('.tag::text').extract()

            items['quote_sentence'] = quote_sentence
            items['author'] = author
            items['tags'] = tags

            yield items

        next_page = response.css('li.next a::attr(href)').get()

        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)
